package cz.vse.fis.simp06.bp.android;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;

import cz.vse.fis.simp06.bp.core.Core;
import cz.vse.fis.simp06.bp.core.benchmarks.AesCbcBenchmark;
import cz.vse.fis.simp06.bp.core.benchmarks.DhBenchmark;
import cz.vse.fis.simp06.bp.core.benchmarks.RsaBenchmark;
import cz.vse.fis.simp06.bp.core.benchmarks.ShaBenchmark;
import cz.vse.fis.simp06.bp.core.benchmarks.ShaWithEcdsaBenchmark;
import cz.vse.fis.simp06.bp.core.benchmarks.ShaWithRsaBenchmark;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.BouncyCastleDh2048;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.BouncyCastleRsa2048;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.ITestableDh;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.ITestableRsa;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.NativeRsa2048;
import cz.vse.fis.simp06.bp.core.implementations.hash.BouncyCastleSha256;
import cz.vse.fis.simp06.bp.core.implementations.hash.GuavaSha256;
import cz.vse.fis.simp06.bp.core.implementations.hash.ITestableSha;
import cz.vse.fis.simp06.bp.core.implementations.hash.NativeSha256;
import cz.vse.fis.simp06.bp.core.implementations.sign.BouncyCastleSha256WithEcdsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.BouncyCastleSha256WithRsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.ITestableShaWithEcdsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.ITestableShaWithRsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.NativeSha256WithEcdsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.NativeSha256WithRsa;
import cz.vse.fis.simp06.bp.core.implementations.sign.TinkSha256WithEcdsa;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.BouncyCastleAes128Cbc;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.CommonsCryptoAes128Cbc;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.ITestableAesCbc;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.NativeAes128Cbc;

/**
 * This class represents the main activity for this application.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class MainActivity extends AppCompatActivity
{
    private Runnable benchRunnable, logcatRunnable;
    private FileObserver logObserver;
    private String observedLogPath;
    private String logsDirPath;
    private static final int PERMISSIONS_REQUEST_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logsDirPath = getExternalFilesDir(null).getParentFile().getPath() + File.separator + "logs";

        //the dir won't exist on the first run
        File logsDir = new File(logsDirPath);
        if (!logsDir.exists())
        {
            logsDir.mkdir();
        }

        TextView v = (TextView) findViewById(R.id.tvLog);
        v.setText("Starting. Please note that if DH benchmark is enabled, the initial safe prime generation might take several minutes.");
        v.setMovementMethod(new ScrollingMovementMethod());

        //this is what calls our main bench method
        ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSIONS_REQUEST_CODE);

        logcatRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                logcatCheck();
            }
        };

        benchRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                benchmark();
            }
        };
    }

    /**
     * Sets up the {@link FileObserver} that watches the log output.
     *
     * @param pathToLogFile Path to the log file.
     */
    private void setUpLogObserver(String pathToLogFile)
    {
        logObserver = new FileObserver(pathToLogFile, FileObserver.MODIFY)
        {
            private TextView tv = (TextView) findViewById(R.id.tvLog);

            @Override
            public void onEvent(int event, @Nullable String path)
            {
                //logback has modified the log file, update our screen
                File f = new File(observedLogPath);
                String parsed = Helper.readAndParseLog(f);

                runOnUiThread(() ->
                {
                    tv.setText(parsed);
                    Helper.scrollTextViewToBottom(tv);
                    //tv.append(finalLine + System.getProperty("line.separator"));
                });
            }
        };

        logObserver.startWatching();
    }

    @Deprecated
    private void logcatCheck()
    {
        TextView v = (TextView) findViewById(R.id.tvLog);

        Process logcatProcess = null;
        Process logcatFlushProcess = null;
        try
        {
            logcatFlushProcess = Runtime.getRuntime().exec("logcat -b all -c");  //flush buffers
            logcatProcess = Runtime.getRuntime().exec("logcat");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(logcatProcess.getInputStream()));

            String line = "";
            while ((line = bufferedReader.readLine()) != null)
            {
                //v.setText(line);
                if (!line.contains("cz.vse.fis"))
                {
                    continue;
                }
                String finalLine = line.replaceFirst(".*]\\s\\p{L}*\\s*(\\w*\\.)*\\w*\\.", "");

                /*runOnUiThread(() ->
                {
                    v.append(Html.fromHtml("<font color=#cc0029>" + "TEST_APPEND" + "</font>"));
                    v.append(Html.fromHtml("<font color=#000000>" + "TEST_APPEND_UNCOLORED" + "</font>"));
                });*/

                /*Spanned toAppend = SpannedString.valueOf(line);
                if (line.contains("["))
                {
                    //for regexes, see Pattern
                    toAppend = Html.fromHtml("<font color=#cc0029>" + line.replaceFirst(".*]\\s\\p{L}*\\s*(\\w*\\.)*\\w*\\.", "") + "</font><br>");
                }
                else
                {
                    toAppend = Html.fromHtml("<font color=#000000>" + line.replaceFirst("(\\d\\d-\\d\\d).*?\\/", "") + "</font><br>");
                }

                Spanned finalToAppend = toAppend;*/
                runOnUiThread(() ->
                {
                    v.append(finalLine + System.getProperty("line.separator"));
                });
            }

        }
        catch (IOException e)
        {

        }
        finally
        {
            if (logcatProcess != null)
            {
                logcatProcess.destroy();
            }

            if (logcatFlushProcess != null)
            {
                logcatFlushProcess.destroy();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (requestCode == PERMISSIONS_REQUEST_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(getApplicationContext(), "Starting", Toast.LENGTH_SHORT).show();

            new Thread(benchRunnable).start();
        }
    }

    /**
     * The main benchmark method.
     */
    private void benchmark()
    {
        try
        {
            Core.initialize();

            //sha           OK
            //aes           OK
            //chacha        NOT SUPPORTED (requires API 28+ or newer BC version, see https://developer.android.com/reference/javax/crypto/Cipher)
            //rsa           OK
            //dh            OK, except: cz.vse.fis.simp06.bp.android I/DHParametersHelper: Generated safe primes: 1450 tries took 1367182ms, I/DHParametersHelper: Generated safe primes: 925 tries took 836616ms
            //sha-rsa       OK
            //sha-ecdsa     OK

            ITestableSha[] shaImplementations = { new NativeSha256(), new GuavaSha256(), /*new ConscryptSha256(),*/ new BouncyCastleSha256() };
            ShaBenchmark shaBenchmark = new ShaBenchmark(shaImplementations);

            ITestableAesCbc[] aesImplementations = { new BouncyCastleAes128Cbc(), /*new ConscryptAes128Cbc(),*/ new CommonsCryptoAes128Cbc(), new NativeAes128Cbc() };
            AesCbcBenchmark aesBenchmark = new AesCbcBenchmark(aesImplementations);

            //ITestableChaCha[] chachaImplementations = {new BouncyCastleChaCha(), /*new ConscryptChaCha(),*/ new NativeChaCha()};
            //ChaChaBenchmark chachaBenchmark = new ChaChaBenchmark(chachaImplementations);

            ITestableRsa[] rsaImplementations = { /*new ConscryptRsa2048(),*/ new NativeRsa2048(), new BouncyCastleRsa2048() };
            RsaBenchmark rsaBenchmark = new RsaBenchmark(rsaImplementations);

            ITestableDh[] dhImplementations = { new BouncyCastleDh2048()/*, new NativeDh2048()*/ };
            DhBenchmark dhBenchmark = new DhBenchmark(dhImplementations);

            ITestableShaWithRsa[] shaWithRsaImplementations = { /*new ConscryptSha256WithRsa(),*/ new NativeSha256WithRsa(), new BouncyCastleSha256WithRsa() };
            ShaWithRsaBenchmark shaWithRsaBenchmark = new ShaWithRsaBenchmark(shaWithRsaImplementations);

            ITestableShaWithEcdsa[] competitors = { new BouncyCastleSha256WithEcdsa(), new NativeSha256WithEcdsa(), /*new ConscryptSha256WithEcdsa(),*/ new TinkSha256WithEcdsa() };
            ShaWithEcdsaBenchmark shaWithEcdsaBenchmark = new ShaWithEcdsaBenchmark(competitors);

            setUpLogObserver(observedLogPath = Helper.findNewestFileInDir(logsDirPath).getPath());

            //prepare args here
            String externalStoragePath = Environment.getExternalStorageDirectory().getPath();
            String testDataPath1 = externalStoragePath + File.separator + "JCryptoBench_testData" + File.separator + "landscape-2711127.mp4";
            String testDataPath2 = externalStoragePath + File.separator + "JCryptoBench_testData" + File.separator + "fireworks.mp4";
            String testDataPath3 = externalStoragePath + File.separator + "JCryptoBench_testData" + File.separator + "hello90.txt";

            String[] args = {"-sha", "5", "-shawarmup", "2", "-aes", "5", "-aeswarmup", "2",
                             "-rsa", "2000", "-rsawarmup", "200",
                             "-dh", "100", "-dhwarmup", "10", "-sharsa", "5", "-sharsawarmup", "2",
                             "-shaecdsa", "5", "-shaecdsawarmup", "2",
                             "-data", testDataPath1 + "," + testDataPath2 + "," + testDataPath3};

            Core.runBenchmarks(externalStoragePath, args, shaBenchmark, aesBenchmark, rsaBenchmark, dhBenchmark, shaWithRsaBenchmark, shaWithEcdsaBenchmark);

            runOnUiThread(() -> showDoneTextView("Benchmarking complete. The log file associated with this run can be found in " + logsDirPath));
        }
        catch (IOException | GeneralSecurityException e)
        {
            runOnUiThread(() -> showDoneTextView("A fatal error occurred. Please try restarting the application."));
        }
        finally
        {
            if (logObserver != null)
            {
                logObserver.stopWatching();
            }
        }
    }

    /**
     * Shows a note to the user that the benchmarks have been completed.
     */
    private void showDoneTextView(String text)
    {
        TextView tv = (TextView) findViewById(R.id.tvDone);
        tv.setText(text);
        tv.setVisibility(View.VISIBLE);
    }
}

