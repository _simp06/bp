package cz.vse.fis.simp06.bp.android;

import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Represents a collection of static methods used by the main activity.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class Helper
{
    /**
     * Finds the newest (latest modified) file in a directory.
     *
     * @param dirPath The path to the directory.
     * @return Returns the file if it is found, otherwise returns null.
     */
    public static File findNewestFileInDir(String dirPath)
    {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        File res = files[0];    //there will always be at least one log file

        long max = files[0].lastModified();
        for (int i = 1; i < files.length; i++)
        {
            if (files[i].lastModified() > max)
            {
                max = files[i].lastModified();
                res = files[i];
            }
        }

        return res;
    }

    /**
     * Reads a file and parses its contents to produce a simplified log output.
     *
     * @param f The file to read. Must use the default logback formatting.
     * @return Returns a string containing the formatted output.
     */
    public static String readAndParseLog(File f)
    {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(f)))
        {
            String s;
            while ((s = br.readLine()) != null)
            {
                sb.append(s.replaceFirst(".*]\\s\\p{L}*\\s*(\\w*\\.)*\\w*\\.", ""))
                        .append(System.getProperty("line.separator"));
            }
        }
        catch (IOException e)
        {
            return "";
        }

        return sb.toString();
    }

    /**
     * Scrolls a {@link TextView} to the bottom.
     *
     * @param tv The text view to scroll down.
     */
    public static void scrollTextViewToBottom(TextView tv)
    {
        while (tv.canScrollVertically(1))
        {
            tv.scrollBy(0, 10);
        }
    }
}
