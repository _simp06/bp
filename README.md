# JCryptoBench

JCryptoBench is a benchmarking application that measures the performance of cryptographic primitive implementations, such as hash functions or ciphers.

## Building

The following tools are required:
- Java Development Kit 8;
- Maven;
- Gradle (only if you want to build the Android example);
- Android SDK API Level 23 (only if you want to build the Android example).

The simplest way to build JCryptoBench is to use its Maven parent project.

```cmd
mvn install
```

This builds the core, Desktop and Raspbian examples.

To build the Android example, use Gradle.

```cmd
cd android
gradle assembleDebug
```

## Usage

To run an implementation of JCryptoBench, navigate to a directory with the `.jar` file.

```cmd
cd desktop\target
java -jar JCryptoBench-Desktop.jar
```

Test data must be provided via launch parameters.

```cmd
cd desktop\target
java -jar JCryptoBench-Desktop.jar -data D:\testDataDirectory\testData1,D:\testDataDirectory\testData2
```

Additionally, default test iteration count can be overriden using launch parameters.

```cmd
cd desktop\target
java -jar JCryptoBench-Desktop.jar -data D:\testDataDirectory\testData1 -sha 100 -shawarmup 10
```