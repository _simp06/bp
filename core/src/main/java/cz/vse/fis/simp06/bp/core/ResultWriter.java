package cz.vse.fis.simp06.bp.core;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a results writer.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ResultWriter
{
    //has to have a ctor with string param to specify path to which results will be saved
    //initialized in host app
    private final String resultsDir;
    private static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    private final Logger logger = LoggerFactory.getLogger(ResultWriter.class);

    /**
     * Initializes a new instance of the class.
     *
     * @param resultsDir The path to the root directory where results will be stored. Do NOT include the trailing {@link File#separator}.
     * @throws IOException Thrown when an exception occurred while creating directories or writing files.
     */
    public ResultWriter(String resultsDir) throws IOException
    {
        //this.resultsDir = resultsDir;
        String timestamp = TIMESTAMP_FORMAT.format(new Date());
        //this.resultsDir = Files.createDirectories(Paths.get(resultsDir, "benchmark_" + timestamp)).toString();
        logger.info("Creating output directories");
        File dir = new File(resultsDir + (resultsDir.equals("") ? "" : File.separator) + Constants.APP_NAME + File.separator + "benchmark_" + timestamp);
        if (!dir.mkdirs())
        {
            logger.error("Failed to create output directories");
            throw new IOException("failed to create output directories");
        }
        this.resultsDir = dir.getPath();
        logger.info("Results will be written in '{}'", this.resultsDir);
    }

    /**
     * Writes a benchmark result to a CSV file.
     *
     * @param benchmarkResult The benchmark result to write.
     * @throws IOException Thrown if an exception occurred while writing to the file.
     */
    public void write(BenchmarkResult benchmarkResult) throws IOException
    {
        //prepare a timestamp
        String timestamp = TIMESTAMP_FORMAT.format(new Date());

        //create a subdir for this run
        //var dir = Files.createDirectory(Paths.get("benchmark_" + timestamp));

        logger.info("Writing results for '{}'", benchmarkResult.benchmarkName);
        //create a file for this run
        File f = new File(resultsDir, benchmarkResult.benchmarkName + "_" + timestamp + ".csv");

        //https://www.baeldung.com/apache-commons-csv
        FileWriter fw = new FileWriter(f);
        //var headers = Arrays.stream(ImplementationResult.class.getFields()).map(Field::getName).toArray(String[]::new);

        try (CSVPrinter printer = new CSVPrinter(fw, CSVFormat.RFC4180.withHeader(OutputFields.class)))
        {
            for (ImplementationResult res : benchmarkResult.results)
            {
                try
                {
                    printer.printRecord(res.implementationName, res.testDataName, res.iterations, res.runtimeMs, res.outputTotalBits, res.note);
                }
                catch (IOException e)
                {
                    logger.error("Failed to write results for '{}'", benchmarkResult.benchmarkName);
                    //e.printStackTrace();
                }
            }
        }

        fw.close();
    }
}
