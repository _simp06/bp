package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.*;

/**
 * This class represents a SHAwithRSA implementation provided by BouncyCastle.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BouncyCastleSha256WithRsa implements ITestableShaWithRsa
{
    private final KeyPair keyPair;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchProviderException  Thrown if the provider is not available.
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public BouncyCastleSha256WithRsa() throws NoSuchProviderException, NoSuchAlgorithmException
    {
        //generate RSA keys
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
        keyGen.initialize(2048);
        keyPair = keyGen.generateKeyPair();
    }

    @Override
    public Pair<byte[], byte[]> sign(byte[] input) throws GeneralSecurityException
    {
        //sign
        Signature sig = Signature.getInstance("SHA256WithRSA", "BC");
        sig.initSign(keyPair.getPrivate());
        sig.update(input);
        byte[] signatureObj = sig.sign();

        return new Pair<>(input, signatureObj);
    }

    @Override
    public boolean verifySignature(byte[] signed, byte[] signatureObj) throws GeneralSecurityException
    {
        Signature sig = Signature.getInstance("SHA256WithRSA", "BC");
        sig.initVerify(keyPair.getPublic());
        sig.update(signed);

        return sig.verify(signatureObj);
    }
}
