package cz.vse.fis.simp06.bp.core;

/**
 * This class contains constant values.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class Constants
{
    //general config
    public static final String APP_NAME = "JCryptoBench";
    public static final String ANDROID_JAVA_VENDOR = "The Android Project";

    //https://github.com/trustin/os-maven-plugin
    //raspberry pi running an on arm8 arch (but 32-bit os) reports its arch as "arm", hopefully this doesn't mess with 64-bit arm archs
    public static final String[] OS_ARCH_32 = { "x8632", "x86", "i386", "i486", "i586", "i686", "ia32", "x32", "arm" };

    //test data
    //NOTE: currently only accepted as a cmd arg
    /*public static final String TEST_DATA_PATH_1 = "testData/hello.txt";
    public static final String TEST_DATA_NAME_1 = "hello.txt";
    public static final String TEST_DATA_PATH_2 = "testData/BuildingsTraffic_Drone.mp4";
    public static final String TEST_DATA_NAME_2 = "BuildingsTraffic_Drone.mp4";*/
    //public static final String TEST_DATA_PATH_3 = "testData/suburban-atmosphere.flac";
    //public static final String TEST_DATA_NAME_3 = "suburban-atmosphere.flac";


    /*public static final HashSet<TestData> TEST_DATA_BUNDLE = new HashSet<TestData>()
    {
        {
            add(new TestData(TEST_DATA_NAME_1, TEST_DATA_PATH_1));
            add(new TestData(TEST_DATA_NAME_2, TEST_DATA_PATH_2));
            add(new TestData(TEST_DATA_NAME_3, TEST_DATA_PATH_3));
        }
    };*/

    //iterations
    //note that this might be overriden in BenchmarkConfig if the user chooses to do so

    //SHA256
    public static final int SHA_ITERATIONS = 10;
    public static final int SHA_ITERATIONS_WARMUP = 5;

    //AES128CBC
    public static final int AES_ITERATIONS = 10;
    public static final int AES_ITERATIONS_WARMUP = 5;

    //CHACHA
    public static final int CHACHA_ITERATIONS = 10;
    public static final int CHACHA_ITERATIONS_WARMUP = 5;

    //DH2048
    public static final int DH_ITERATIONS = 1000;
    public static final int DH_ITERATIONS_WARMUP = 100;

    //RSA2048
    public static final int RSA_ITERATIONS = 5000;
    public static final int RSA_ITERATIONS_WARMUP = 500;

    //SHA-ECDSA
    public static final int SHA_WITH_ECDSA_ITERATIONS = 10;
    public static final int SHA_WITH_ECDSA_ITERATIONS_WARMUP = 5;

    //SHA-RSA
    public static final int SHA_WITH_RSA_ITERATIONS = 10;
    public static final int SHA_WITH_RSA_ITERATIONS_WARMUP = 5;

    /**
     * Hides the constructor as this is a utility class.
     */
    private Constants()
    {

    }
}
