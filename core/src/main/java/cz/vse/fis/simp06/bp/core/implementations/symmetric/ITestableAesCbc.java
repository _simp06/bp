package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Specifies an interface for AES block cipher implementations.
 * The key length is 128 bits, mode of operation CBC, padding PKCS7 (PKCS5 in the transformation).
 * The secret key is stored within the class and as such data encrypted with an instance of the class must be decrypted with the same instance.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableAesCbc
{
    /**
     * Encrypts an input.
     *
     * @param input The input data.
     * @return Returns a {@link Pair} with the encrypted result and an {@link IvParameterSpec} needed to decrypt the data.
     * The output is represented by {@link Pair#x}, while {@link Pair#y} is the {@link IvParameterSpec}.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     * @throws IOException              Thrown if there was a general I/O problem.
     */
    Pair<byte[], IvParameterSpec> encrypt(byte[] input) throws GeneralSecurityException, IOException;

    /**
     * Decrypts an encrypted input.
     *
     * @param encrypted The encrypted input.
     * @param iv        The {@link IvParameterSpec} returned by the encryption method.
     * @return Returns the decrypted data.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     * @throws IOException              Thrown if there was a general I/O problem.
     */
    byte[] decrypt(byte[] encrypted, IvParameterSpec iv) throws GeneralSecurityException, IOException;
}
