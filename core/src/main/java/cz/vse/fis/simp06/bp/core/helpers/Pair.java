package cz.vse.fis.simp06.bp.core.helpers;

import java.util.Objects;

/**
 * This class represents a 2-tuple, a pair.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class Pair<T1, T2>
{
    public final T1 x;
    public final T2 y;

    /**
     * Initializes a new instance of the class.
     *
     * @param x The first value.
     * @param y The second value.
     */
    public Pair(T1 x, T2 y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(x, pair.x) &&
                Objects.equals(y, pair.y);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(x, y);
    }
}
