package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * This class represents a built in AES-128-CBC implementation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class NativeAes128Cbc implements ITestableAesCbc
{
    private final SecretKey key;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public NativeAes128Cbc() throws NoSuchAlgorithmException
    {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        keygen.init(128);
        key = keygen.generateKey();
    }

    @Override
    public Pair<byte[], IvParameterSpec> encrypt(byte[] input) throws GeneralSecurityException
    {
        SecureRandom rnd = new SecureRandom();
        byte[] ivBytes = new byte[16];
        rnd.nextBytes(ivBytes);

        IvParameterSpec iv = new IvParameterSpec(ivBytes);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

        return new Pair<>(cipher.doFinal(input), iv);
    }

    @Override
    public byte[] decrypt(byte[] encrypted, IvParameterSpec iv) throws GeneralSecurityException
    {
        //decrypt
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, iv);
        return cipher.doFinal(encrypted);
    }
}
