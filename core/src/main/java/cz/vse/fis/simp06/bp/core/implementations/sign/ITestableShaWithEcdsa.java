package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.GeneralSecurityException;

/**
 * Specifies an interface for SHA256withECDSA signature algorithm implementations.
 * The hash function used is the SHA-256 (SHA-2 family).
 * The secp256r1 curve, also known as prime256v1 or P-256 is used.
 * The key pair is stored within the class and as such signatures made with an instance of the class must be verified by the same instance.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableShaWithEcdsa
{
    /**
     * Produces a signature on the input data.
     *
     * @param input The input data to sign.
     * @return Returns the signature as well as the original input in a {@link Pair}.
     * The original input is represented by {@link Pair#x}, while {@link Pair#y} is the signature object.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     */
    Pair<byte[], byte[]> sign(byte[] input) throws GeneralSecurityException;

    /**
     * Verifies a signature.
     *
     * @param signed       The data that was signed.
     * @param signatureObj The signature.
     * @return Returns true if the signature is valid, otherwise returns false.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     */
    boolean verifySignature(byte[] signed, byte[] signatureObj) throws GeneralSecurityException;
}
