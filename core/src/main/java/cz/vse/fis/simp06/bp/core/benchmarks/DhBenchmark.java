package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.ITestableDh;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for Diffie-Hellman key exchange implementations.
 * See {@link ITestableDh} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class DhBenchmark implements IBenchmark
{
    private final ITestableDh[] implementations;
    private final Logger logger = LoggerFactory.getLogger(DhBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    private long bytesTotal = 0;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public DhBenchmark(ITestableDh[] implementations)
    {
        this.implementations = implementations;
    }

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        logger.info("Starting DH benchmark");

        for (ITestableDh implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing DH benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, ITestableDh implementation, boolean warmup)
    {
        int iterations = warmup ? benchmarkConfig.DH_ITERATIONS_WARMUP : benchmarkConfig.DH_ITERATIONS;

        benchmarkInnerLoop(iterations, implementation, warmup);
    }

    /**
     * Encapsulates the full testing loops.
     *  @param iterations Number of iterations in testing.
     * @param isWarmup   Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkInnerLoop(int iterations, ITestableDh implementation, boolean isWarmup)
    {
        logger.info("Implementation '{}' starting", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                Pair<byte[], byte[]> res = implementation.computeSharedKeys();
                bytesTotal += res.x.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("GeneralSecurityException thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = bytesTotal * 8;   //the key pair returned is just a shared key on both sides
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), "", iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "");
        bytesTotal = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }
}
