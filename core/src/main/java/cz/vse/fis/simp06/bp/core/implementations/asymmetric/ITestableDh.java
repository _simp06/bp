package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Specifies an interface for Diffie-Hellman key exchange implementations. Key length is 2048 bits.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableDh
{
    /**
     * Performs the Diffie-Hellman key exchange.
     *
     * @return Returns a pair of keys each representing the secret key computed on one side.
     * Note that if those keys aren't equal, the implementantion is incorrect.
     * @throws NoSuchProviderException  Thrown if the provider for this implementation cannot be found.
     * @throws NoSuchAlgorithmException Thrown if the algorithm is not properly specified.
     * @throws InvalidKeyException      Thrown if the keys generated are invalid.
     */
    Pair<byte[], byte[]> computeSharedKeys() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException;
}
