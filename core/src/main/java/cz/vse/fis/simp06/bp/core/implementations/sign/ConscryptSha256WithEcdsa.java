package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.*;
import java.security.spec.ECGenParameterSpec;

/**
 * This class represents a SHAwithECDSA implementation provided by Conscrypt.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ConscryptSha256WithEcdsa implements ITestableShaWithEcdsa
{
    private final KeyPair keyPair;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchProviderException            Thrown if the provider is not available.
     * @throws NoSuchAlgorithmException           Thrown if the algorithm spec is not available.
     * @throws InvalidAlgorithmParameterException Thrown if the parameter is invalid for the EC key generator.
     */
    public ConscryptSha256WithEcdsa() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC", "Conscrypt");
        keyGen.initialize(new ECGenParameterSpec("secp256r1"));
        keyPair = keyGen.generateKeyPair();
    }

    @Override
    public Pair<byte[], byte[]> sign(byte[] input) throws GeneralSecurityException
    {
        //sign
        Signature sig = Signature.getInstance("SHA256WithECDSA", "Conscrypt");
        sig.initSign(keyPair.getPrivate());
        sig.update(input);
        byte[] signatureObj = sig.sign();

        return new Pair<>(input, signatureObj);
    }

    @Override
    public boolean verifySignature(byte[] signed, byte[] signatureObj) throws GeneralSecurityException
    {
        Signature sig = Signature.getInstance("SHA256WithECDSA", "Conscrypt");
        sig.initVerify(keyPair.getPublic());
        sig.update(signed);

        return sig.verify(signatureObj);
    }
}
