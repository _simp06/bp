package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.*;

/**
 * This class represents a built in SHAwithRSA implementation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class NativeSha256WithRsa implements ITestableShaWithRsa
{
    private final KeyPair keyPair;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public NativeSha256WithRsa() throws NoSuchAlgorithmException
    {
        //generate RSA keys
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        keyPair = keyGen.generateKeyPair();
    }

    @Override
    public Pair<byte[], byte[]> sign(byte[] input) throws GeneralSecurityException
    {
        //sign
        Signature sig = Signature.getInstance("SHA256WithRSA");
        sig.initSign(keyPair.getPrivate());
        sig.update(input);
        byte[] signatureObj = sig.sign();

        return new Pair<>(input, signatureObj);
    }

    @Override
    public boolean verifySignature(byte[] signed, byte[] signatureObj) throws GeneralSecurityException
    {
        Signature sig = Signature.getInstance("SHA256WithRSA");
        sig.initVerify(keyPair.getPublic());
        sig.update(signed);

        return sig.verify(signatureObj);
    }
}
