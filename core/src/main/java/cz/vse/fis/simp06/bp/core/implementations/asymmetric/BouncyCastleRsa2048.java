package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import javax.crypto.Cipher;
import java.security.*;

/**
 * This class represents an RSA implementation provided by BouncyCastle.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BouncyCastleRsa2048 implements ITestableRsa
{
    private final KeyPair keyPair;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchProviderException  Thrown if the provider is not available.
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public BouncyCastleRsa2048() throws NoSuchProviderException, NoSuchAlgorithmException
    {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
        keyGen.initialize(2048);
        keyPair = keyGen.generateKeyPair();
    }

    @Override
    public byte[] encrypt(byte[] input) throws GeneralSecurityException /*NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException*/
    {
        //http://www.java2s.com/Tutorial/Java/0490__Security/BasicRSAexample.htm
        //https://www.mobilefish.com/download/bouncycastle/PublicExample.java

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        return cipher.doFinal(input);
    }

    @Override
    public byte[] decrypt(byte[] encrypted) throws GeneralSecurityException /*InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException*/
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

        return cipher.doFinal(encrypted);
    }
}
