package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import javax.crypto.KeyAgreement;
import java.security.*;

/**
 * This class represents a Diffie-Hellman key exchange implementation provided by BouncyCastle.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BouncyCastleDh2048 implements ITestableDh
{
    private final KeyPair keyPair1;
    private final KeyPair keyPair2;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchProviderException  Thrown if the provider is not available.
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public BouncyCastleDh2048() throws NoSuchProviderException, NoSuchAlgorithmException
    {
        //each side generates a key pair
        KeyPairGenerator keyPairGenerator1 = KeyPairGenerator.getInstance("DH", "BC");
        keyPairGenerator1.initialize(2048);
        keyPair1 = keyPairGenerator1.generateKeyPair();

        KeyPairGenerator keyPairGenerator2 = KeyPairGenerator.getInstance("DH", "BC");
        keyPairGenerator2.initialize(2048);
        keyPair2 = keyPairGenerator2.generateKeyPair();
    }

    @Override
    public Pair<byte[], byte[]> computeSharedKeys() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException
    {
        //each side computes a shared secret key
        KeyAgreement keyAgreement1 = KeyAgreement.getInstance("DH", "BC");
        keyAgreement1.init(keyPair1.getPrivate());
        keyAgreement1.doPhase(keyPair2.getPublic(), true);

        byte[] secretKey1 = keyAgreement1.generateSecret();

        KeyAgreement keyAgreement2 = KeyAgreement.getInstance("DH", "BC");
        keyAgreement2.init(keyPair2.getPrivate());
        keyAgreement2.doPhase(keyPair1.getPublic(), true);

        byte[] secretKey2 = keyAgreement2.generateSecret();

        return new Pair<>(secretKey1, secretKey2);
    }
}
