package cz.vse.fis.simp06.bp.core.implementations.hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class represents a built in SHA-256 (SHA-2 family) implementation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class NativeSha256 implements ITestableSha
{
    @Override
    public byte[] hash(byte[] input) throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance("SHA-256").digest(input);
    }
}
