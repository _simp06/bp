package cz.vse.fis.simp06.bp.core.implementations.hash;

import com.google.common.hash.Hashing;

/**
 * This class represents a SHA-256 (SHA-2 family) implementation provided by Guava.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class GuavaSha256 implements ITestableSha
{
    @Override
    public byte[] hash(byte[] input)
    {
        return Hashing.sha256().hashBytes(input).asBytes();
    }
}
