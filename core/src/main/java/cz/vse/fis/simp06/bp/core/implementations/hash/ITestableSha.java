package cz.vse.fis.simp06.bp.core.implementations.hash;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Specifies an interface for SHA-256 (SHA-2 family) hash function implementations.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableSha
{
    /**
     * Produces a hash (message digest) of an input (a message).
     *
     * @param input The data to hash.
     * @return Returns the hash.
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     * @throws NoSuchProviderException  Thrown if the provider is not available.
     */
    byte[] hash(byte[] input) throws NoSuchAlgorithmException, NoSuchProviderException;
}
