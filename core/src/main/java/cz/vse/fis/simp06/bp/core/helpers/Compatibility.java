package cz.vse.fis.simp06.bp.core.helpers;

import cz.vse.fis.simp06.bp.core.Constants;

/**
 * This class represents a collection of static methods used to adjust functionality based on platform.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class Compatibility
{
    /**
     * Detects platform specification to determine whether to use Conscrypt as a security provider.
     *
     * @return Returns true if Conscrypt can be used, otherwise returns false.
     */
    public static boolean useConscrypt()
    {
        //no point in testing conscrypt on android, see https://boringssl.googlesource.com/boringssl/
        if (System.getProperty("java.vendor").equals(Constants.ANDROID_JAVA_VENDOR))
        {
            return false;
        }

        //conscrypt doesn't work on 32-bit linux (https://github.com/google/conscrypt/)
        if (System.getProperty("os.name").toLowerCase().contains("linux"))
        {
            String currArch = System.getProperty("os.arch");
            for (String arch : Constants.OS_ARCH_32)
            {
                if (currArch.equals(arch))
                {
                    return false;
                }
            }
        }


        return true;
    }
}
