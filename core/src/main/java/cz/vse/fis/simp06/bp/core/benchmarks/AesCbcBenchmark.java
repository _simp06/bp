package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.ITestableAesCbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for AES implementations.
 * See {@link ITestableAesCbc} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class AesCbcBenchmark implements IBenchmark
{
    private final ITestableAesCbc[] implementations;
    private final Logger logger = LoggerFactory.getLogger(AesCbcBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    /*public final ArrayList<Pair<byte[], IvParameterSpec>> encOutputStore = new ArrayList();
    public final ArrayList<byte[]> decOutputStore = new ArrayList();*/
    private long encBytes = 0;
    private long decBytes = 0;
    //private Pair<byte[], IvParameterSpec> preEncrypted;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public AesCbcBenchmark(ITestableAesCbc[] implementations)
    {
        this.implementations = implementations;
    }

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        /*implementationResults.clear();

        logger.info("Warming up");
        for (TestData testData : Constants.TEST_DATA_BUNDLE)
        {
            benchmarkLoop(benchmarkConfig.AES_ITERATIONS_WARMUP, testData.load(), testData.dataName, true);
        }

        logger.info("Warmed up, running benchmarks");
        for (TestData testData : Constants.TEST_DATA_BUNDLE)
        {
            benchmarkLoop(benchmarkConfig.AES_ITERATIONS, testData.load(), testData.dataName, true);
        }

        logger.info("Benchmark complete, collecting results");*/

        //implementationResults.clear();

        /*logger.info("Warming up");
        benchmarkLoop(benchmarkConfig.AES_ITERATIONS_WARMUP, testData, true);

        logger.info("Running timed benchmark");
        benchmarkLoop(benchmarkConfig.AES_ITERATIONS, testData, false);*/

        //warmup right before timed

        logger.info("Starting AES benchmark with test data '{}'", testData.dataName);

        for (ITestableAesCbc implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, testData, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, testData, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing AES benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    /**
     * Starts the benchmark inner loops.
     *
     * @param benchmarkConfig The configuration of the benchmark.
     * @param testData Test data used in the benchmark.
     * @param implementation The implementation tested.
     * @param warmup Specifies whether this is a warmup or a timed run.
     */
    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, TestData testData, ITestableAesCbc implementation, boolean warmup)
    {
        int iterations = warmup ? benchmarkConfig.AES_ITERATIONS_WARMUP : benchmarkConfig.AES_ITERATIONS;

        Pair<byte[], IvParameterSpec> preEncrypted = benchmarkEncryptLoop(iterations, testData, implementation, warmup);
        benchmarkDecryptLoop(iterations, preEncrypted, testData.dataName, implementation, warmup);
    }

    /**
     * Tests an implementation in terms of encryption.
     *
     * @param iterations     Number of iterations in testing.
     * @param testData       The data which will be used for testing.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     * @return Returns pre-encrypted data.
     */
    private Pair<byte[], IvParameterSpec> benchmarkEncryptLoop(int iterations, TestData testData, ITestableAesCbc implementation, boolean isWarmup)
    {
        logger.info("Loading test data '{}'", testData.dataName);
        byte[] data;
        try
        {
            data = testData.load();
        }
        catch (IOException e)
        {
            logger.error("Failed to load test data");
            return null;
        }

        logger.info("Implementation '{}' starting, encrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                Pair<byte[], IvParameterSpec> enc = implementation.encrypt(data);
                encBytes += enc.x.length;
            }
            catch (GeneralSecurityException | IOException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = encBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testData.dataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "encryption");
        encBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }

        logger.info("Pre-encrypting test data for decryption test");
        return preEncrypt(data, implementation);
    }

    /**
     * Tests an implementation in terms of decryption.
     *  @param iterations     Number of iterations in testing.
     * @param preEncrypted   Pre-encrypted data used in this test.
     * @param testDataName   Name of the test data.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkDecryptLoop(int iterations, Pair<byte[], IvParameterSpec> preEncrypted, String testDataName, ITestableAesCbc implementation, boolean isWarmup)
    {
        logger.info("Implementation '{}' starting, decrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                byte[] dec = implementation.decrypt(preEncrypted.x, preEncrypted.y);
                decBytes += dec.length;   //NOTE: this is an attempt at preventing dead code elimination, see https://en.wikipedia.org/wiki/Dead_code
            }
            catch (GeneralSecurityException | IOException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = decBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testDataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "decryption");
        decBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }

    /**
     * Encrypts test data in advance for the decryption part of the testing.
     *
     * @param testData       The data to encrypt.
     * @param implementation The implementation used to perform the encryption.
     * @return Returns the pre-encrypted data.
     */
    private Pair<byte[], IvParameterSpec> preEncrypt(final byte[] testData, ITestableAesCbc implementation)
    {
        try
        {
            return implementation.encrypt(testData);
        }
        catch (GeneralSecurityException | IOException e)
        {
            logger.error("Failed to pre-encrypt data for decryption test");
            return null;
            //e.printStackTrace();
        }
    }
}
