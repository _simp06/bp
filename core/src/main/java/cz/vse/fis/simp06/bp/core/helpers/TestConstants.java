package cz.vse.fis.simp06.bp.core.helpers;

/**
 * This class contains constant values used in unit tests.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class TestConstants
{
    //sha2-256
    //see:  https://csrc.nist.gov/projects/cryptographic-standards-and-guidelines/example-values
    //      https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA256.pdf
    //pair.x is the input message, pair.y is the resulting message digest
    public static final Pair<String, String> oneBlock = new Pair<>("abc", "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad");
    public static final Pair<String, String> twoBlocks = new Pair<>("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
            "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1");

    //ciphers
    public static final String plainText = "abc";

    /**
     * Hides the constructor as this is a utility class.
     */
    private TestConstants()
    {

    }
}
