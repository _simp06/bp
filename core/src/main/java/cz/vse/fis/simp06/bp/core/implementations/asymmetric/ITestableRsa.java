package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import java.security.GeneralSecurityException;

/**
 * Specifies an interface for RSA implementations. Key length is 2048 bits. PKCS1-v1_5 padding is used.
 * The key pair is stored within the class and as such data encrypted with an instance of the class must be decrypted with the same instance.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableRsa
{
    /**
     * Performs an encryption.
     *
     * @param input The input data. Note that the input must be at most 245 bytes long.
     *              See https://tools.ietf.org/html/rfc3447#section-7.2.1 for more info.
     * @return Returns the encrypted data as byte array.
     * @throws GeneralSecurityException Thrown if there was a problem with the implementation tested.
     */
    byte[] encrypt(byte[] input) throws GeneralSecurityException;

    /**
     * Performs a decryption.
     *
     * @param encrypted A byte array of encrypted data.
     * @return Returns a byte array of decrypted data.
     * @throws GeneralSecurityException Thrown if there was a problem with the implementation tested.
     */
    byte[] decrypt(byte[] encrypted) throws GeneralSecurityException;
}
