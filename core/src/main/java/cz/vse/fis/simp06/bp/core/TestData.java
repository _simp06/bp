package cz.vse.fis.simp06.bp.core;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class represents a test data encapsulation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class TestData
{
    public final String dataName;
    public final String dataPath;

    /**
     * Initializes a new instance of the class.
     *
     * @param name Name of the test data.
     * @param path Path to the test data.
     */
    public TestData(String name, String path)
    {
        dataName = name;
        dataPath = path;
    }

    /**
     * Loads the test data into memory.
     *
     * @return Returns the test data as a byte array.
     * @throws IOException Thrown if there was an error loading the data.
     */
    public byte[] load() throws IOException
    {
        try
        {
            InputStream is = getClass().getClassLoader().getResourceAsStream(dataPath);
            return ByteStreams.toByteArray(is);
        }
        catch (NullPointerException e)
        {
            //it's not a resource but an external file
            return Files.toByteArray(new File(dataPath));
        }
    }
}
