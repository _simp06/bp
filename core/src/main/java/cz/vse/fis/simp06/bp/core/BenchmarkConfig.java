package cz.vse.fis.simp06.bp.core;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import cz.vse.fis.simp06.bp.core.helpers.ParameterValidator;

import java.util.List;

/**
 * This class represents a user-specified benchmark configuration.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BenchmarkConfig
{
    //public static boolean OVERRIDE_DEFAULT_CONFIG = false;

    //custom test data

    //usage: -data file1,file2
    @Parameter(names = "-data")
    public List<String> TEST_DATA_PATHS;

    //NOTE: those are meant to be "constants", but JCommander doesn't like the final keyword

    //SHA256
    @Parameter(names = "-sha", description = "Number of SHA iterations", validateWith = ParameterValidator.class)
    public int SHA_ITERATIONS = Constants.SHA_ITERATIONS;
    @Parameter(names = "-shawarmup", description = "Number of SHA warmup iterations", validateWith = ParameterValidator.class)
    public int SHA_ITERATIONS_WARMUP = Constants.SHA_ITERATIONS_WARMUP;

    //AES128CBC
    @Parameter(names = "-aes", description = "Number of AES iterations", validateWith = ParameterValidator.class)
    public int AES_ITERATIONS = Constants.AES_ITERATIONS;
    @Parameter(names = "-aeswarmup", description = "Number of AES warmup iterations", validateWith = ParameterValidator.class)
    public int AES_ITERATIONS_WARMUP = Constants.AES_ITERATIONS_WARMUP;

    //CHACHA
    @Parameter(names = "-chacha", description = "Number of CHACHA iterations", validateWith = ParameterValidator.class)
    public int CHACHA_ITERATIONS = Constants.CHACHA_ITERATIONS;
    @Parameter(names = "-chachawarmup", description = "Number of CHACHA warmup iterations", validateWith = ParameterValidator.class)
    public int CHACHA_ITERATIONS_WARMUP = Constants.CHACHA_ITERATIONS_WARMUP;

    //DH2048
    @Parameter(names = "-dh", description = "Number of DH iterations", validateWith = ParameterValidator.class)
    public int DH_ITERATIONS = Constants.DH_ITERATIONS;
    @Parameter(names = "-dhwarmup", description = "Number of DH warmup iterations", validateWith = ParameterValidator.class)
    public int DH_ITERATIONS_WARMUP = Constants.DH_ITERATIONS_WARMUP;

    //RSA2048
    @Parameter(names = "-rsa", description = "Number of RSA iterations", validateWith = ParameterValidator.class)
    public int RSA_ITERATIONS = Constants.RSA_ITERATIONS;
    @Parameter(names = "-rsawarmup", description = "Number of RSA warmup iterations", validateWith = ParameterValidator.class)
    public int RSA_ITERATIONS_WARMUP = Constants.RSA_ITERATIONS_WARMUP;

    //SHA-ECDSA
    @Parameter(names = "-shaecdsa", description = "Number of SHAwithECDSA iterations", validateWith = ParameterValidator.class)
    public int SHA_WITH_ECDSA_ITERATIONS = Constants.SHA_WITH_ECDSA_ITERATIONS;
    @Parameter(names = "-shaecdsawarmup", description = "Number of SHAwithECDSA warmup iterations", validateWith = ParameterValidator.class)
    public int SHA_WITH_ECDSA_ITERATIONS_WARMUP = Constants.SHA_WITH_ECDSA_ITERATIONS_WARMUP;

    //SHA-RSA
    @Parameter(names = "-sharsa", description = "Number of SHAwithRSA iterations", validateWith = ParameterValidator.class)
    public int SHA_WITH_RSA_ITERATIONS = Constants.SHA_WITH_RSA_ITERATIONS;
    @Parameter(names = "-sharsawarmup", description = "Number of SHAwithRSA warmup iterations", validateWith = ParameterValidator.class)
    public int SHA_WITH_RSA_ITERATIONS_WARMUP = Constants.SHA_WITH_RSA_ITERATIONS_WARMUP;

    /**
     * Initializes a new instance of the class.
     *
     * @param args The arguments string as passed into a main method.
     * @return Returns a new instance of the class.
     */
    public static BenchmarkConfig benchmarkConfigInit(String[] args)
    {
        //OVERRIDE_DEFAULT_CONFIG = true;

        BenchmarkConfig bco = new BenchmarkConfig();

        JCommander.newBuilder().addObject(bco).build().parse(args);

        return bco;
    }

    /**
     * Hides the constructor as this is a utility class.
     */
    private BenchmarkConfig()
    {

    }
}
