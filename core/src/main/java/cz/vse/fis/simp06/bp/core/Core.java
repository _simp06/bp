package cz.vse.fis.simp06.bp.core;

import com.beust.jcommander.ParameterException;
import com.google.common.io.Files;
import com.google.crypto.tink.config.TinkConfig;
import cz.vse.fis.simp06.bp.core.benchmarks.IBenchmark;
import cz.vse.fis.simp06.bp.core.helpers.Compatibility;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.conscrypt.OpenSSLProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.util.HashSet;

/**
 * This class provides a general API of this library.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class Core
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Core.class);

    /**
     * Initializes the environment to enable benchmarks to run.
     *
     * @throws GeneralSecurityException Thrown when there was an issue setting up security providers or other security related configuration.
     */
    public static void initialize() throws GeneralSecurityException
    {
        LOGGER.info("Initializing JCryptoBench environment");
        LOGGER.info("Adding Bouncy Castle provider");
        Security.addProvider(new BouncyCastleProvider());
        if (Compatibility.useConscrypt())
        {
            LOGGER.info("Adding Conscrypt provider");
            Security.addProvider(new OpenSSLProvider());
        }

        LOGGER.info("Registering Tink config");
        TinkConfig.register();

        LOGGER.info("If Diffie-Hellman benchmark is enabled, a number of safe primes has to be generated first on some devices. This may take a while.");
    }

    /**
     * Runs all benchmarks provided to this method and saves the results.
     *
     * @param resultsDirPath The directory where results will be written to.
     * @param args           Arguments passed to the calling main method.
     * @param benchmarks     The benchmarks to be run.
     * @throws IOException        Thrown when there was an issue in an implementation run.
     * @throws ParameterException Thrown if one or more parameters are invalid.
     */
    public static void runBenchmarks(String resultsDirPath, String[] args, IBenchmark... benchmarks) throws IOException, ParameterException
    {
        LOGGER.info("Parsing arguments");
        BenchmarkConfig bco = BenchmarkConfig.benchmarkConfigInit(args);

        if (bco.TEST_DATA_PATHS == null)
        {
            LOGGER.error("No test data specified, exiting");
            return;
        }

        ResultWriter resultWriter = new ResultWriter(resultsDirPath);

        //look if we have custom test data
        HashSet<TestData> testDataBundle = new HashSet<>();

        //make a new data bundle
        for (String path : bco.TEST_DATA_PATHS)
        {
            String name = Files.getNameWithoutExtension(path) + "." + Files.getFileExtension(path);
            testDataBundle.add(new TestData(name, path));
        }

        LOGGER.info("Starting benchmarks");
        for (TestData testData : testDataBundle)
        {
            /*LOGGER.info("Loading test data '{}'", testData.dataName);
            byte[] loadedData = testData.load();
            LOGGER.info("Test data '{}' loaded", testData.dataName);*/

            for (IBenchmark bench : benchmarks)
            {
                bench.run(bco, testData);
            }

            //System.gc();
        }

        //done, don't forget to pick up results

        for (IBenchmark benchmark : benchmarks)
        {
            resultWriter.write(benchmark.getResults());
        }

        LOGGER.info("Completed all benchmarks");
    }

    /*
    public static void runBenchmarks(String resultsDirPath, String[] args, ITestData testDataManager, IBenchmark... benchmarks) throws IOException, ParameterException
    {
        LOGGER.info("Parsing arguments");
        BenchmarkConfig bco = BenchmarkConfig.benchmarkConfigInit(args);

        if (bco.TEST_DATA_PATHS == null)
        {
            LOGGER.error("No test data specified, exiting");
            return;
        }

        ResultWriter resultWriter = new ResultWriter(resultsDirPath);

        //look if we have custom test data
        HashSet<ITestData> testDataBundle = new HashSet<>();

        //make a new data bundle
        for (String path : bco.TEST_DATA_PATHS)
        {
            String name = Files.getNameWithoutExtension(path) + "." + Files.getFileExtension(path);
            testDataBundle.add(testDataManager.createNew(name, path));
        }

        LOGGER.info("Starting benchmarks");
        for (ITestData testData : testDataBundle)
        {
            LOGGER.info("Loading test data '{}'", testData.getName());
            byte[] loadedData = testData.load();
            LOGGER.info("Test data '{}' loaded", testData.getName());

            for (IBenchmark bench : benchmarks)
            {
                bench.run(bco, loadedData, testData.getName());
            }

            testData.cleanUp();
        }

        //done, don't forget to pick up results

        for (IBenchmark benchmark : benchmarks)
        {
            resultWriter.write(benchmark.getResults());
        }

        LOGGER.info("Completed all benchmarks");
    }
    */
}
