package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import javax.crypto.KeyAgreement;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * This class represents a built in Diffie-Hellman key exchange implementation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class NativeDh2048 implements ITestableDh
{
    private final KeyPair keyPair1;
    private final KeyPair keyPair2;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public NativeDh2048() throws NoSuchAlgorithmException
    {
        //each side generates a key pair
        KeyPairGenerator keyPairGenerator1 = KeyPairGenerator.getInstance("DH");
        keyPairGenerator1.initialize(2048);
        keyPair1 = keyPairGenerator1.generateKeyPair();

        KeyPairGenerator keyPairGenerator2 = KeyPairGenerator.getInstance("DH");
        keyPairGenerator2.initialize(2048);
        keyPair2 = keyPairGenerator2.generateKeyPair();
    }

    @Override
    public Pair<byte[], byte[]> computeSharedKeys() throws NoSuchAlgorithmException, InvalidKeyException
    {
        //each side computes a shared secret key
        KeyAgreement keyAgreement1 = KeyAgreement.getInstance("DH");
        keyAgreement1.init(keyPair1.getPrivate());
        keyAgreement1.doPhase(keyPair2.getPublic(), true);

        byte[] secretKey1 = keyAgreement1.generateSecret();

        KeyAgreement keyAgreement2 = KeyAgreement.getInstance("DH");
        keyAgreement2.init(keyPair2.getPrivate());
        keyAgreement2.doPhase(keyPair1.getPublic(), true);

        byte[] secretKey2 = keyAgreement2.generateSecret();

        return new Pair<>(secretKey1, secretKey2);
    }
}
