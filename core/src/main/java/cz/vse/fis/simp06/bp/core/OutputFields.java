package cz.vse.fis.simp06.bp.core;

/**
 * Enumerates fields used in the CSV results file.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public enum OutputFields
{
    IMPLEMENTATION_NAME, TEST_DATA_NAME, ITERATIONS, RUNTIME_MS, OUTPUT_BITS, NOTE
}
