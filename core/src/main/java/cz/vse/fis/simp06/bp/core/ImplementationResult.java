package cz.vse.fis.simp06.bp.core;

import java.util.Objects;

/**
 * This class represents a result of an implementation within a benchmark.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ImplementationResult
{
    public final String implementationName;
    public final String testDataName;
    public final int iterations;
    public final long runtimeMs;
    public final long outputTotalBits;
    public final String note;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementationName Name of the implementation.
     * @param testDataName       Name of the data used in a particular test.
     * @param iterations         Specifies the amount of iterations for a test loop.
     * @param runtimeMs          Specifies the total runtime in milliseconds.
     * @param outputTotalBits    Specifies the total output volume in bits.
     * @param note               Represents a custom note associated with the test.
     */
    public ImplementationResult(String implementationName, String testDataName, int iterations, long runtimeMs, long outputTotalBits, String note)
    {
        this.implementationName = implementationName;
        this.testDataName = testDataName;
        this.iterations = iterations;
        this.runtimeMs = runtimeMs;
        this.outputTotalBits = outputTotalBits;
        this.note = note;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImplementationResult that = (ImplementationResult) o;
        return iterations == that.iterations &&
                runtimeMs == that.runtimeMs &&
                outputTotalBits == that.outputTotalBits &&
                implementationName.equals(that.implementationName) &&
                testDataName.equals(that.testDataName) &&
                Objects.equals(note, that.note);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(implementationName, testDataName, iterations, runtimeMs, outputTotalBits, note);
    }
}
