package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.implementations.hash.ITestableSha;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for SHA implementations.
 * See {@link ITestableSha} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ShaBenchmark implements IBenchmark
{
    private final ITestableSha[] implementations;
    private final Logger logger = LoggerFactory.getLogger(ShaBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    private long bytesTotal = 0;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public ShaBenchmark(ITestableSha[] implementations)
    {
        this.implementations = implementations;
    }
    //NOTE: bc vs jce speed see http://bouncy-castle.1462172.n4.nabble.com/SHA1-speed-and-correctness-td4656567.html

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        //implementationResults.clear();

        logger.info("Starting SHA benchmark with test data '{}'", testData.dataName);

        for (ITestableSha implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, testData, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, testData, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing SHA benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, TestData testData, ITestableSha implementation, boolean warmup)
    {
        int iterations = warmup ? benchmarkConfig.SHA_ITERATIONS_WARMUP : benchmarkConfig.SHA_ITERATIONS;

        benchmarkInnerLoop(iterations, testData, implementation, warmup);
    }

    /**
     * Encapsulates the full testing loops.
     *  @param iterations Number of iterations in testing.
     * @param testData   The data which will be used for testing.
     * @param isWarmup   Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkInnerLoop(int iterations, TestData testData, ITestableSha implementation, boolean isWarmup)
    {
        logger.info("Loading test data '{}'", testData.dataName);
        byte[] data;
        try
        {
            data = testData.load();
        }
        catch (IOException e)
        {
            logger.error("Failed to load test data");
            return;
        }

        logger.info("Implementation '{}' starting", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                byte[] res = implementation.hash(data);
                bytesTotal += res.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("GeneralSecurityException thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = bytesTotal * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testData.dataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "");
        bytesTotal = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }
}
