package cz.vse.fis.simp06.bp.core.helpers;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * This class represents a parameter validator for numeric values.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ParameterValidator implements IParameterValidator
{
    @Override
    public void validate(String s, String s1) throws ParameterException
    {
        try
        {
            int val = Integer.parseInt(s1);

            if (val < 0)
            {
                throw new ParameterException("Parameter " + s + " must be a non-negative integer.");
            }
        }
        catch (NumberFormatException e)
        {
            throw new ParameterException("Parameter " + s + " must be a non-negative integer.");
        }

    }
}
