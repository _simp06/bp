package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.implementations.sign.ITestableShaWithRsa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for SHAwithRSA implementations.
 * See {@link ITestableShaWithRsa} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ShaWithRsaBenchmark implements IBenchmark
{
    private final ITestableShaWithRsa[] implementations;
    private final Logger logger = LoggerFactory.getLogger(ShaWithRsaBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    private long encBytes = 0;
    private final ArrayList<Boolean> decOutputStore = new ArrayList();
    //private Pair<byte[], byte[]> preEncrypted;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public ShaWithRsaBenchmark(ITestableShaWithRsa[] implementations)
    {
        this.implementations = implementations;
    }

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        //implementationResults.clear();

        logger.info("Starting SHAwithRSA benchmark with test data '{}'", testData.dataName);

        for (ITestableShaWithRsa implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, testData, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, testData, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing SHAwithRSA benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, TestData testData, ITestableShaWithRsa implementation, boolean isWarmup)
    {
        int iterations = isWarmup ? benchmarkConfig.SHA_WITH_RSA_ITERATIONS_WARMUP : benchmarkConfig.SHA_WITH_RSA_ITERATIONS;

        Pair<byte[], byte[]> preSigned = benchmarkSignLoop(iterations, testData, implementation, isWarmup);
        benchmarkVerifyLoop(iterations, preSigned, testData.dataName, implementation, isWarmup);

        decOutputStore.clear();
    }

    /**
     * Tests an implementation in terms of signing.
     *
     * @param iterations     Number of iterations in testing.
     * @param testData       The data which will be used for testing.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     * @return Returns pre-signed data to be used in the signature verification test.
     */
    private Pair<byte[], byte[]> benchmarkSignLoop(int iterations, TestData testData, ITestableShaWithRsa implementation, boolean isWarmup)
    {
        logger.info("Loading test data '{}'", testData.dataName);
        byte[] data;
        try
        {
            data = testData.load();
        }
        catch (IOException e)
        {
            logger.error("Failed to load test data");
            return null;
        }

        logger.info("Implementation '{}' starting, sign mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                Pair<byte[], byte[]> enc = implementation.sign(data);
                encBytes += enc.y.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = encBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testData.dataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "signing");
        encBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }

        logger.info("Pre-signing test data for signature verification test");
        return preSign(data, implementation);
    }

    /**
     * Tests an implementation in terms of verifying signatures.
     *  @param iterations     Number of iterations in testing.
     * @param preSigned      Pre-signed data used in this test.
     * @param testDataName   Name of the test data.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkVerifyLoop(int iterations, Pair<byte[], byte[]> preSigned, String testDataName, ITestableShaWithRsa implementation, boolean isWarmup)
    {
        logger.info("Implementation '{}' starting, verify mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                boolean dec = implementation.verifySignature(preSigned.x, preSigned.y);
                decOutputStore.add(dec);   //NOTE: this is an attempt at preventing dead code elimination, see https://en.wikipedia.org/wiki/Dead_code
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        //var bitsTotal = decOutputStore.stream().mapToInt(x -> x.length).sum() * 8;
        boolean resultsOk = true;
        for (Boolean x : decOutputStore)
        {
            if (!x)
            {
                logger.error("A signature could not be verified, results will be meaningless.");
                resultsOk = false;
            }
        }
        String note = resultsOk ? "verification, output not measured" : "BAD SIGNATURE DETECTED - verification";
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testDataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), 0, note);

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }

    /**
     * Signs test data in advance for the signature verification part of the testing.
     *
     * @param testData       The data to sign.
     * @param implementation The implementation used to perform the signing.
     * @return Returns the pre-signed data.
     */
    private Pair<byte[], byte[]> preSign(final byte[] testData, ITestableShaWithRsa implementation)
    {
        try
        {
            return implementation.sign(testData);
        }
        catch (GeneralSecurityException e)
        {
            logger.error("Failed to pre-sign data for signature verification test");
            return null;
            //e.printStackTrace();
        }
    }
}
