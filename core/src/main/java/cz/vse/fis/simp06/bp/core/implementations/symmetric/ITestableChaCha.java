package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;

/**
 * Specifies an interface for ChaCha stream cipher implementations.
 * The 20 rounds variant is used, with 256-bit keys.
 * The secret key is stored within the class and as such data encrypted with an instance of the class must be decrypted with the same instance.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface ITestableChaCha
{
    /**
     * Encrypts an input.
     *
     * @param input The input data.
     * @return Returns a {@link Pair} with the encrypted result and an {@link AlgorithmParameterSpec} needed to decrypt the data.
     * The output is represented by {@link Pair#x}, while {@link Pair#y} is the {@link AlgorithmParameterSpec}.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     */
    Pair<byte[], AlgorithmParameterSpec> encrypt(byte[] input) throws GeneralSecurityException;

    /**
     * Decrypts an encrypted input.
     *
     * @param encrypted The encrypted input.
     * @param spec      The {@link AlgorithmParameterSpec} returned by the encryption method.
     * @return Returns the decrypted data.
     * @throws GeneralSecurityException Thrown if there was a general problem.
     */
    byte[] decrypt(byte[] encrypted, AlgorithmParameterSpec spec) throws GeneralSecurityException;
}
