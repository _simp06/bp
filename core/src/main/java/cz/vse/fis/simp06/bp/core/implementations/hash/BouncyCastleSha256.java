package cz.vse.fis.simp06.bp.core.implementations.hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * This class represents a SHA-256 (SHA-2 family) implementation provided by BouncyCastle.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BouncyCastleSha256 implements ITestableSha
{
    @Override
    public byte[] hash(byte[] input) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return MessageDigest.getInstance("SHA-256", "BC").digest(input);
    }
}
