package cz.vse.fis.simp06.bp.core.implementations.sign;

import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.PublicKeySign;
import com.google.crypto.tink.PublicKeyVerify;
import com.google.crypto.tink.signature.SignatureKeyTemplates;
import cz.vse.fis.simp06.bp.core.helpers.Pair;

import java.security.GeneralSecurityException;

/**
 * This class represents a SHAwithECDSA implementation provided by Tink.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class TinkSha256WithEcdsa implements ITestableShaWithEcdsa
{
    private final KeysetHandle privateKeysetHandle;

    /**
     * Initializes a new instance of the class.
     *
     * @throws GeneralSecurityException Thrown if there is a general problem with setting up the environment.
     */
    public TinkSha256WithEcdsa() throws GeneralSecurityException
    {
        privateKeysetHandle = KeysetHandle.generateNew(SignatureKeyTemplates.ECDSA_P256);
    }

    @Override
    public Pair<byte[], byte[]> sign(byte[] input) throws GeneralSecurityException
    {
        PublicKeySign signer = privateKeysetHandle.getPrimitive(PublicKeySign.class);
        byte[] signature = signer.sign(input);

        return new Pair<>(input, signature);
    }

    @Override
    public boolean verifySignature(byte[] signed, byte[] signatureObj) throws GeneralSecurityException
    {
        KeysetHandle publicKeysetHandle = privateKeysetHandle.getPublicKeysetHandle();
        PublicKeyVerify verifier = publicKeysetHandle.getPrimitive(PublicKeyVerify.class);

        try
        {
            verifier.verify(signatureObj, signed);
            return true;
        }
        catch (GeneralSecurityException e)
        {
            return false;
        }
    }
}
