package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.ITestableChaCha;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for CHACHA implementations.
 * See {@link ITestableChaCha} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class ChaChaBenchmark implements IBenchmark
{
    private final ITestableChaCha[] implementations;
    private final Logger logger = LoggerFactory.getLogger(ChaChaBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    private long encBytes = 0;
    private long decBytes = 0;
    //private Pair<byte[], AlgorithmParameterSpec> preEncrypted;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public ChaChaBenchmark(ITestableChaCha[] implementations)
    {
        this.implementations = implementations;
    }

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        //implementationResults.clear();

        logger.info("Starting CHACHA benchmark with test data '{}'", testData.dataName);

        for (ITestableChaCha implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, testData, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, testData, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing CHACHA benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    /**
     * Starts the benchmark inner loops.
     *
     * @param benchmarkConfig The configuration of the benchmark.
     * @param testData Test data used in the benchmark.
     * @param implementation The implementation tested.
     * @param warmup Specifies whether this is a warmup or a timed run.
     */
    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, TestData testData, ITestableChaCha implementation, boolean warmup)
    {
        int iterations = warmup ? benchmarkConfig.CHACHA_ITERATIONS_WARMUP : benchmarkConfig.CHACHA_ITERATIONS;

        Pair<byte[], AlgorithmParameterSpec> preEncrypted = benchmarkEncryptLoop(iterations, testData, implementation, warmup);
        benchmarkDecryptLoop(iterations, preEncrypted, testData.dataName, implementation, warmup);
    }

    /**
     * Tests an implementation in terms of encryption.
     *
     * @param iterations     Number of iterations in testing.
     * @param testData       The data which will be used for testing.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     * @return Returns pre-encrypted data to be used in the decryption test.
     */
    private Pair<byte[], AlgorithmParameterSpec> benchmarkEncryptLoop(int iterations, TestData testData, ITestableChaCha implementation, boolean isWarmup)
    {
        logger.info("Loading test data '{}'", testData.dataName);
        byte[] data;
        try
        {
            data = testData.load();
        }
        catch (IOException e)
        {
            logger.error("Failed to load test data");
            return null;
        }

        logger.info("Implementation '{}' starting, encrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                Pair<byte[], AlgorithmParameterSpec> enc = implementation.encrypt(data);
                encBytes += enc.x.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = encBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testData.dataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "encryption");
        encBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }

        logger.info("Pre-encrypting test data for decryption test");
        return preEncrypt(data, implementation);
    }

    /**
     * Tests an implementation in terms of decryption.
     *  @param iterations     Number of iterations in testing.
     * @param preEncrypted   Pre-encrypted data used in this test.
     * @param testDataName   Name of the test data.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkDecryptLoop(int iterations, Pair<byte[], AlgorithmParameterSpec> preEncrypted, String testDataName, ITestableChaCha implementation, boolean isWarmup)
    {
        logger.info("Implementation '{}' starting, decrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                byte[] dec = implementation.decrypt(preEncrypted.x, preEncrypted.y);
                decBytes += dec.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = decBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testDataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "decryption");
        decBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }

    /**
     * Encrypts test data in advance for the decryption part of the testing.
     *
     * @param testData       The data to encrypt.
     * @param implementation The implementation used to perform the encryption.
     * @return Returns the pre-encrypted data.
     */
    private Pair<byte[], AlgorithmParameterSpec> preEncrypt(final byte[] testData, ITestableChaCha implementation)
    {
        try
        {
            return implementation.encrypt(testData);
        }
        catch (GeneralSecurityException e)
        {
            logger.error("Failed to pre-encrypt data for decryption test");
            return null;
            //e.printStackTrace();
        }
    }
}
