package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.AlgorithmParameterSpec;

/**
 * This class represents a ChaCha20 implementation provided by BouncyCastle.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BouncyCastleChaCha implements ITestableChaCha
{
    private final SecretKey key;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchProviderException  Thrown if the provider is not available.
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public BouncyCastleChaCha() throws NoSuchProviderException, NoSuchAlgorithmException
    {
        KeyGenerator keygen = KeyGenerator.getInstance("ChaCha", "BC");
        keygen.init(256);
        key = keygen.generateKey();
    }

    @Override
    public Pair<byte[], AlgorithmParameterSpec> encrypt(byte[] input) throws GeneralSecurityException
    {
        Cipher c = Cipher.getInstance("ChaCha", "BC"); //https://openjdk.java.net/jeps/329
        c.init(Cipher.ENCRYPT_MODE, key);   //nonce and counter generated internally
        IvParameterSpec spec = new IvParameterSpec(c.getIV());

        return new Pair<>(c.doFinal(input), spec);
    }

    @Override
    public byte[] decrypt(byte[] encrypted, AlgorithmParameterSpec spec) throws GeneralSecurityException
    {
        Cipher cipher = Cipher.getInstance("ChaCha", "BC");
        cipher.init(Cipher.DECRYPT_MODE, key, spec);

        return cipher.doFinal(encrypted);
    }
}
