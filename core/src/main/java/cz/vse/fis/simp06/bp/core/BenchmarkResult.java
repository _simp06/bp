package cz.vse.fis.simp06.bp.core;

import java.util.ArrayList;

/**
 * This class represents result data of a benchmark.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class BenchmarkResult
{
    public final String benchmarkName;
    public final ArrayList<ImplementationResult> results;

    /**
     * Initializes a new instance of the class.
     *
     * @param benchmarkName Name of the benchmark.
     * @param results       A set of results of all implementations tested in the benchmark.
     */
    public BenchmarkResult(String benchmarkName, ArrayList<ImplementationResult> results)
    {
        this.benchmarkName = benchmarkName;
        this.results = results;
    }
}
