package cz.vse.fis.simp06.bp.core.benchmarks;

import com.google.common.base.Stopwatch;
import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.ImplementationResult;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.ITestableRsa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a benchmark for RSA implementations.
 * See {@link ITestableRsa} for implementation details.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class RsaBenchmark implements IBenchmark
{
    private final ITestableRsa[] implementations;
    private final Logger logger = LoggerFactory.getLogger(RsaBenchmark.class);
    private ArrayList<ImplementationResult> implementationResults = new ArrayList<>();

    //this is the max input length for this cipher variant, see comments in the run method below
    private static final int MAX_INPUT_LENGTH = 245;

    private long encBytes = 0;
    private long decBytes = 0;
    //private byte[] preEncrypted;

    //just hardcoded, too much trouble with resources pathing
    /*private static final long TEST_DATA_LENGTH_1 = 300000000;
    private static final long TEST_DATA_LENGTH_2 = 225175061;
    private static final long TEST_DATA_LENGTH_3 = 101129596;*/
    private int runCnt = 0;
    private String testDataName = "";
    private static final int INPUT_SIZE_SUB = 25;

    /**
     * Initializes a new instance of the class.
     *
     * @param implementations An array of implementations to be tested.
     */
    public RsaBenchmark(ITestableRsa[] implementations)
    {
        this.implementations = implementations;
    }

    @Override
    public void run(BenchmarkConfig benchmarkConfig, TestData testData)
    {
        //implementationResults.clear();

        //NOTE: because this is RSA with a 2048 bit key size, we need to rescale input data to 245 bytes assuming PKCS1v1.5 padding
        //      https://tools.ietf.org/html/rfc3447#section-7.2.1 says:
        //       1. Length checking: If mLen > k - 11, output "message too long" and
        //          stop. (where mLen is input length, k is public key size in bytes (modulus))

        if (!testDataName.equals(testData.dataName))
        {
            runCnt++;
            testDataName = testData.dataName;
        }

        logger.info("Starting RSA benchmark with test data '{}'", testData.dataName);

        for (ITestableRsa implementation : implementations)
        {
            logger.info("Warming up");
            benchmarkLoop(benchmarkConfig, testData, implementation, true);

            logger.info("Running timed benchmark");
            benchmarkLoop(benchmarkConfig, testData, implementation, false);
        }

        //hopefully get a "clean" memory state before the next benchmark starts
        System.gc();
        logger.info("Finishing RSA2048 benchmark");
    }

    @Override
    public BenchmarkResult getResults()
    {
        return new BenchmarkResult(getClass().getSimpleName(), implementationResults);
    }

    /**
     * Starts the benchmark inner loops.
     *
     * @param benchmarkConfig The configuration of the benchmark.
     * @param testData Test data used in the benchmark.
     * @param implementation The implementation tested.
     * @param warmup Specifies whether this is a warmup or a timed run.
     */
    private void benchmarkLoop(BenchmarkConfig benchmarkConfig, TestData testData, ITestableRsa implementation, boolean warmup)
    {
        int iterations = warmup ? benchmarkConfig.RSA_ITERATIONS_WARMUP : benchmarkConfig.RSA_ITERATIONS;

        byte[] preEncrypted = benchmarkEncryptLoop(iterations, testData, implementation, warmup);
        benchmarkDecryptLoop(iterations, preEncrypted, testData.dataName, implementation, warmup);
    }

    /**
     * Scales the test data so that its valid for an RSA encryption.
     *
     * @param testData The test data to be scaled.
     * @return Returns the scaled data.
     */
    private byte[] rescaleTestData(final byte[] testData)
    {
        return Arrays.copyOf(testData, MAX_INPUT_LENGTH - (runCnt - 1) * INPUT_SIZE_SUB);
    }

    /**
     * Tests an implementation in terms of encryption.
     *
     * @param iterations     Number of iterations in testing.
     * @param testData       The data which will be used for testing.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     * @return Returns pre-encrypted data to be used in the decryption test.
     */
    private byte[] benchmarkEncryptLoop(int iterations, TestData testData, ITestableRsa implementation, boolean isWarmup)
    {
        logger.info("Loading test data '{}'", testData.dataName);
        byte[] data;
        try
        {
            data = rescaleTestData(testData.load());
        }
        catch (IOException e)
        {
            logger.error("Failed to load test data");
            return null;
        }

        logger.info("Implementation '{}' starting, encrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                byte[] enc = implementation.encrypt(data);
                encBytes += enc.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = encBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testData.dataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "encryption");
        encBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }

        logger.info("Pre-encrypting test data for decryption test");
        return preEncrypt(data, implementation);
    }

    /**
     * Tests an implementation in terms of decryption.
     *  @param iterations     Number of iterations in testing.
     * @param preEncrypted   Pre-encrypted data used in this test.
     * @param testDataName   Name of the test data.
     * @param implementation The implementation which is tested in this loop.
     * @param isWarmup       Specifies whether this benchmark loop is a warmup one.
     */
    private void benchmarkDecryptLoop(int iterations, byte[] preEncrypted, String testDataName, ITestableRsa implementation, boolean isWarmup)
    {
        logger.info("Implementation '{}' starting, decrypt mode", implementation.getClass().getSimpleName());
        Stopwatch s = Stopwatch.createStarted();
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                byte[] dec = implementation.decrypt(preEncrypted);
                decBytes += dec.length;
            }
            catch (GeneralSecurityException e)
            {
                logger.error("Exception thrown, results will be meaningless.");
                //e.printStackTrace();
            }
        }
        s.stop();
        logger.info("Elapsed time: {} ms", s.elapsed(TimeUnit.MILLISECONDS));

        long bitsTotal = decBytes * 8;
        ImplementationResult result = new ImplementationResult(implementation.getClass().getSimpleName(), testDataName, iterations, s.elapsed(TimeUnit.MILLISECONDS), bitsTotal, "decryption");
        decBytes = 0;

        if (!isWarmup)
        {
            implementationResults.add(result);
        }
    }

    /**
     * Encrypts test data in advance for the decryption part of the testing.
     *
     * @param testData       The data to encrypt.
     * @param implementation The implementation used to perform the encryption.
     * @return Returns the pre-encrypted data.
     */
    private byte[] preEncrypt(final byte[] testData, ITestableRsa implementation)
    {
        try
        {
            return implementation.encrypt(testData);
        }
        catch (GeneralSecurityException e)
        {
            logger.error("Failed to pre-encrypt data for decryption test");
            return null;
            //e.printStackTrace();
        }
    }
}
