package cz.vse.fis.simp06.bp.core.benchmarks;

import cz.vse.fis.simp06.bp.core.BenchmarkConfig;
import cz.vse.fis.simp06.bp.core.BenchmarkResult;
import cz.vse.fis.simp06.bp.core.TestData;

/**
 * Specifies an interface for benchmarks
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public interface IBenchmark
{
    /**
     * Runs the benchmark.
     *
     * @param benchmarkConfig Config overriding the default values, specified by the caller.
     * @param testData        Data the test is performed on.
     */
    void run(BenchmarkConfig benchmarkConfig, TestData testData);

    /**
     * Gets cumulative results for all benchmark runs performed on this instance.
     *
     * @return Returns a {@link BenchmarkResult} containing results of all implementations tested.
     */
    BenchmarkResult getResults();
}
