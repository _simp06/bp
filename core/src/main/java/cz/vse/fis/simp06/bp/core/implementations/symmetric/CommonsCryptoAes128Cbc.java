package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import org.apache.commons.crypto.cipher.CryptoCipher;
import org.apache.commons.crypto.utils.Utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Properties;

/**
 * This class represents an AES-128-CBC implementation provided by Apache Commons Crypto.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class CommonsCryptoAes128Cbc implements ITestableAesCbc
{
    //see: http://commons.apache.org/proper/commons-crypto/userguide.html
    //     http://commons.apache.org/proper/commons-crypto/xref-test/org/apache/commons/crypto/examples/CipherByteArrayExample.html

    private Properties properties = new Properties();
    private final SecretKey key;
    private int outputBufferSize;
    private int inputLength;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public CommonsCryptoAes128Cbc() throws NoSuchAlgorithmException
    {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        keygen.init(128);
        key = keygen.generateKey();
    }

    @Override
    public Pair<byte[], IvParameterSpec> encrypt(byte[] input) throws GeneralSecurityException, IOException
    {
        //properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.OPENSSL.getClassName());

        //NOTE: we add 16 to length because of padding to match AES block size -- PKCS7 will add up to 15 bytes of padding if input is not a multiple of 16 and 16 bytes if it is
        outputBufferSize = (inputLength = input.length) + 16;

        SecureRandom rnd = new SecureRandom();
        byte[] ivBytes = new byte[16];
        rnd.nextBytes(ivBytes);

        IvParameterSpec iv = new IvParameterSpec(ivBytes);

        CryptoCipher encipher = Utils.getCipherInstance("AES/CBC/PKCS5Padding", properties);
        encipher.init(Cipher.ENCRYPT_MODE, key, iv);

        byte[] output = new byte[outputBufferSize];
        int updateBytes = encipher.update(input, 0, input.length, output, 0);

        //We must call doFinal at the end of encryption/decryption.
        int finalBytes = encipher.doFinal(input, 0, 0, output, updateBytes);
        encipher.close();

        //NOTE: this truncates the array, so we no longer need the output length as its equal to the truncated array's length
        return new Pair<>(Arrays.copyOf(output, updateBytes + finalBytes), iv);
    }

    //NOTE: AlgorithmParameters vs AlgorithmParameterSpec see this https://en.wikipedia.org/wiki/Opaque_data_type
    @Override
    public byte[] decrypt(byte[] encrypted, IvParameterSpec iv) throws GeneralSecurityException, IOException
    {
        //decrypt
        //properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.JCE.getClassName());

        //NOTE: this used to be implemented a bit differently, see last commit: b0bd5791af8ab4b133c1f17934cf8487eaffc4d4

        CryptoCipher decipher = Utils.getCipherInstance("AES/CBC/PKCS5Padding", properties);
        decipher.init(Cipher.DECRYPT_MODE, key, iv);
        byte[] decrypted = new byte[outputBufferSize + 16];
        decipher.doFinal(encrypted, 0, encrypted.length, decrypted, 0);

        return Arrays.copyOf(decrypted, inputLength);
    }
}
