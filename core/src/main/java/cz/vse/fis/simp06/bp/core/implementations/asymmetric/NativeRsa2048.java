package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * This class represents a built in RSA implementation.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class NativeRsa2048 implements ITestableRsa
{
    private final KeyPair keyPair;

    /**
     * Initializes a new instance of the class.
     *
     * @throws NoSuchAlgorithmException Thrown if the algorithm spec is not available.
     */
    public NativeRsa2048() throws NoSuchAlgorithmException
    {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        keyPair = keyGen.generateKeyPair();
    }

    @Override
    public byte[] encrypt(byte[] input) throws GeneralSecurityException /*NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException*/
    {
        //http://www.java2s.com/Tutorial/Java/0490__Security/BasicRSAexample.htm
        //https://www.mobilefish.com/download/bouncycastle/PublicExample.java

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        return cipher.doFinal(input);
    }

    @Override
    public byte[] decrypt(byte[] encrypted) throws GeneralSecurityException /*InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException*/
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

        return cipher.doFinal(encrypted);
    }
}
