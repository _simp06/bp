package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.conscrypt.OpenSSLProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link ConscryptRsa2048}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class ConscryptRsa2048Test
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new OpenSSLProvider());
    }

    /**
     * Tests encryption and decryption operations as specified by {@link ITestableRsa}.
     */
    @Test
    void testEncDec()
    {
        try
        {
            byte[] data = TestConstants.plainText.getBytes();

            ConscryptRsa2048 impl = new ConscryptRsa2048();
            byte[] enc = impl.encrypt(data);
            byte[] dec = impl.decrypt(enc);

            assertArrayEquals(data, dec);
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}