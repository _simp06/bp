package cz.vse.fis.simp06.bp.core.implementations.sign;

import com.google.crypto.tink.config.TinkConfig;
import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link TinkSha256WithEcdsa}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class TinkSha256WithEcdsaTest
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        try
        {
            TinkConfig.register();
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }

    /**
     * Tests signing and verifying signatures as specified by {@link ITestableShaWithEcdsa}.
     */
    @Test
    void testSignVerify()
    {
        try
        {
            TinkSha256WithEcdsa impl = new TinkSha256WithEcdsa();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], byte[]> signed = impl.sign(data);

            assertTrue(impl.verifySignature(data, signed.y));

            data[0] = 'x';

            assertFalse(impl.verifySignature(data, signed.y));
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}