package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link BouncyCastleSha256WithRsa}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class BouncyCastleSha256WithRsaTest
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Tests signing and verifying signatures as specified by {@link ITestableShaWithRsa}.
     */
    @Test
    void testSignVerify()
    {
        try
        {
            BouncyCastleSha256WithRsa impl = new BouncyCastleSha256WithRsa();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], byte[]> signed = impl.sign(data);

            assertTrue(impl.verifySignature(data, signed.y));

            data[0] = 'x';

            assertFalse(impl.verifySignature(data, signed.y));
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}