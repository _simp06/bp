package cz.vse.fis.simp06.bp.core.implementations.hash;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.apache.commons.codec.binary.Hex;
import org.conscrypt.OpenSSLProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link ConscryptSha256}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class ConscryptSha256Test
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new OpenSSLProvider());
    }

    /**
     * Tests hashing as specified by {@link ITestableSha}.
     *
     * @see TestConstants
     */
    @Test
    void testHash()
    {
        ConscryptSha256 impl = new ConscryptSha256();

        try
        {
            assertEquals(TestConstants.oneBlock.y, Hex.encodeHexString(impl.hash(TestConstants.oneBlock.x.getBytes())));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e)
        {
            fail();
        }

        try
        {
            assertEquals(TestConstants.twoBlocks.y, Hex.encodeHexString(impl.hash(TestConstants.twoBlocks.x.getBytes())));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e)
        {
            fail();
        }
    }
}