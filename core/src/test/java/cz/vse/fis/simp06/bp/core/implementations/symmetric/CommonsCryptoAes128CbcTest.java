package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.GeneralSecurityException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link CommonsCryptoAes128Cbc}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class CommonsCryptoAes128CbcTest
{
    /**
     * Tests encryption and decryption operations as specified by {@link ITestableAesCbc}.
     */
    @Test
    void testEncDec()
    {
        try
        {
            CommonsCryptoAes128Cbc impl = new CommonsCryptoAes128Cbc();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], IvParameterSpec> enc = impl.encrypt(data);
            byte[] dec = impl.decrypt(enc.x, enc.y);

            assertArrayEquals(data, dec);
        }
        catch (GeneralSecurityException | IOException e)
        {
            fail();
        }
    }
}