package cz.vse.fis.simp06.bp.core.implementations.hash;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link NativeSha256}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class NativeSha256Test
{
    /**
     * Tests hashing as specified by {@link ITestableSha}.
     *
     * @see TestConstants
     */
    @Test
    void testHash()
    {
        NativeSha256 impl = new NativeSha256();

        try
        {
            assertEquals(TestConstants.oneBlock.y, Hex.encodeHexString(impl.hash(TestConstants.oneBlock.x.getBytes())));
        }
        catch (NoSuchAlgorithmException e)
        {
            fail();
        }

        try
        {
            assertEquals(TestConstants.twoBlocks.y, Hex.encodeHexString(impl.hash(TestConstants.twoBlocks.x.getBytes())));
        }
        catch (NoSuchAlgorithmException e)
        {
            fail();
        }
    }
}