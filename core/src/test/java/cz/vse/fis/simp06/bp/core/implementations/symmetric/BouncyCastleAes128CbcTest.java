package cz.vse.fis.simp06.bp.core.implementations.symmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.IvParameterSpec;
import java.security.GeneralSecurityException;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link BouncyCastleAes128Cbc}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class BouncyCastleAes128CbcTest
{
    /**
     * Adds the provider required for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Tests encryption and decryption operations as specified by {@link ITestableAesCbc}.
     */
    @Test
    void testEncDec()
    {
        try
        {
            BouncyCastleAes128Cbc impl = new BouncyCastleAes128Cbc();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], IvParameterSpec> enc = impl.encrypt(data);
            byte[] dec = impl.decrypt(enc.x, enc.y);

            assertArrayEquals(data, dec);
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}