package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link NativeSha256WithRsa}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class NativeSha256WithRsaTest
{
    /**
     * Tests signing and verifying signatures as specified by {@link ITestableShaWithRsa}.
     */
    @Test
    void testSignVerify()
    {
        try
        {
            NativeSha256WithRsa impl = new NativeSha256WithRsa();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], byte[]> signed = impl.sign(data);

            assertTrue(impl.verifySignature(data, signed.y));

            data[0] = 'x';

            assertFalse(impl.verifySignature(data, signed.y));
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}