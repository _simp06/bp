package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link NativeRsa2048}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class NativeRsa2048Test
{
    /**
     * Tests encryption and decryption operations as specified by {@link ITestableRsa}.
     */
    @Test
    void testEncDec()
    {
        try
        {
            byte[] data = TestConstants.plainText.getBytes();

            NativeRsa2048 impl = new NativeRsa2048();
            byte[] enc = impl.encrypt(data);
            byte[] dec = impl.decrypt(enc);

            assertArrayEquals(data, dec);
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}