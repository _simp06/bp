package cz.vse.fis.simp06.bp.core.implementations.sign;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link BouncyCastleSha256WithEcdsa}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class BouncyCastleSha256WithEcdsaTest
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Tests signing and verifying signatures as specified by {@link ITestableShaWithEcdsa}.
     */
    @Test
    void testSignVerify()
    {
        try
        {
            BouncyCastleSha256WithEcdsa impl = new BouncyCastleSha256WithEcdsa();

            byte[] data = TestConstants.plainText.getBytes();
            Pair<byte[], byte[]> signed = impl.sign(data);

            assertTrue(impl.verifySignature(data, signed.y));

            data[0] = 'x';

            assertFalse(impl.verifySignature(data, signed.y));
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}