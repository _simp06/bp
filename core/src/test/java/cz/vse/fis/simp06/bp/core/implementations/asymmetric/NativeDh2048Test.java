package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import org.junit.jupiter.api.Test;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link NativeDh2048}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class NativeDh2048Test
{
    /**
     * Tests computation of shared keys as specified by {@link ITestableDh}.
     */
    @Test
    void testComputeSharedKeys()
    {
        try
        {
            NativeDh2048 impl = new NativeDh2048();
            Pair<byte[], byte[]> res = impl.computeSharedKeys();

            assertArrayEquals(res.x, res.y);
        }
        catch (NoSuchAlgorithmException | InvalidKeyException e)
        {
            fail();
        }
    }
}