package cz.vse.fis.simp06.bp.core.implementations.hash;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link GuavaSha256}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class GuavaSha256Test
{
    /**
     * Tests hashing as specified by {@link ITestableSha}.
     *
     * @see TestConstants
     */
    @Test
    void testHash()
    {
        GuavaSha256 impl = new GuavaSha256();

        assertEquals(TestConstants.oneBlock.y, Hex.encodeHexString(impl.hash(TestConstants.oneBlock.x.getBytes())));

        assertEquals(TestConstants.twoBlocks.y, Hex.encodeHexString(impl.hash(TestConstants.twoBlocks.x.getBytes())));
    }
}