package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.Pair;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link BouncyCastleDh2048}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class BouncyCastleDh2048Test
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Tests computation of shared keys as specified by {@link ITestableDh}.
     */
    @Test
    void testComputeSharedKeys()
    {
        try
        {
            BouncyCastleDh2048 impl = new BouncyCastleDh2048();
            Pair<byte[], byte[]> res = impl.computeSharedKeys();

            assertArrayEquals(res.x, res.y);
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidKeyException e)
        {
            fail();
        }
    }
}