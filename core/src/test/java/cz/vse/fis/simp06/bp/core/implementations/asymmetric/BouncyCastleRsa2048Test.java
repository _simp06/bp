package cz.vse.fis.simp06.bp.core.implementations.asymmetric;

import cz.vse.fis.simp06.bp.core.helpers.TestConstants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class represents a unit test for {@link BouncyCastleRsa2048}.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
class BouncyCastleRsa2048Test
{
    /**
     * Performs the setup necessary for this implementation to run.
     */
    @BeforeEach
    void setUp()
    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Tests encryption and decryption operations as specified by {@link ITestableRsa}.
     */
    @Test
    void testEncDec()
    {
        try
        {
            byte[] data = TestConstants.plainText.getBytes();

            BouncyCastleRsa2048 impl = new BouncyCastleRsa2048();
            byte[] enc = impl.encrypt(data);
            byte[] dec = impl.decrypt(enc);

            assertArrayEquals(data, dec);
        }
        catch (GeneralSecurityException e)
        {
            fail();
        }
    }
}