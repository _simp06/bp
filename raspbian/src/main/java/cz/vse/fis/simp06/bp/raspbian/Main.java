package cz.vse.fis.simp06.bp.raspbian;

import cz.vse.fis.simp06.bp.core.Core;
import cz.vse.fis.simp06.bp.core.TestData;
import cz.vse.fis.simp06.bp.core.benchmarks.*;
import cz.vse.fis.simp06.bp.core.implementations.asymmetric.*;
import cz.vse.fis.simp06.bp.core.implementations.hash.*;
import cz.vse.fis.simp06.bp.core.implementations.sign.*;
import cz.vse.fis.simp06.bp.core.implementations.symmetric.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;

/**
 * This class represents the main entry point of the application.
 *
 * @author Petr Šimandl
 * @version 1.0.0
 */
public class Main
{
    /**
     * The main method.
     *
     * @param args The commandline arguments passed to the method.
     */
    public static void main(String[] args)
    {
        try
        {
            Core.initialize();

            ITestableSha[] shaImplementations = { new NativeSha256(), new GuavaSha256(), /*new ConscryptSha256(),*/ new BouncyCastleSha256() };
            ShaBenchmark shaBenchmark = new ShaBenchmark(shaImplementations);

            ITestableAesCbc[] aesImplementations = { new BouncyCastleAes128Cbc(), /*new ConscryptAes128Cbc(),*/ new CommonsCryptoAes128Cbc(), new NativeAes128Cbc() };
            AesCbcBenchmark aesBenchmark = new AesCbcBenchmark(aesImplementations);

            ITestableChaCha[] chachaImplementations = { new BouncyCastleChaCha(), /*new ConscryptChaCha(),*/ };
            ChaChaBenchmark chachaBenchmark = new ChaChaBenchmark(chachaImplementations);

            ITestableRsa[] rsaImplementations = { /*new ConscryptRsa2048(),*/ new NativeRsa2048(), new BouncyCastleRsa2048() };
            RsaBenchmark rsaBenchmark = new RsaBenchmark(rsaImplementations);

            ITestableDh[] dhImplementations = { new BouncyCastleDh2048(), new NativeDh2048() };
            DhBenchmark dhBenchmark = new DhBenchmark(dhImplementations);

            ITestableShaWithRsa[] shaWithRsaImplementations = { /*new ConscryptSha256WithRsa(),*/ new NativeSha256WithRsa(), new BouncyCastleSha256WithRsa() };
            ShaWithRsaBenchmark shaWithRsaBenchmark = new ShaWithRsaBenchmark(shaWithRsaImplementations);

            ITestableShaWithEcdsa[] competitors = { new BouncyCastleSha256WithEcdsa(), new NativeSha256WithEcdsa(), /*new ConscryptSha256WithEcdsa(),*/ new TinkSha256WithEcdsa() };
            ShaWithEcdsaBenchmark shaWithEcdsaBenchmark = new ShaWithEcdsaBenchmark(competitors);

            Core.runBenchmarks("", args, shaBenchmark, aesBenchmark, chachaBenchmark, rsaBenchmark, dhBenchmark, shaWithRsaBenchmark, shaWithEcdsaBenchmark);
        }
        catch (IOException | GeneralSecurityException e)
        {
            e.printStackTrace();
        }
    }
}
